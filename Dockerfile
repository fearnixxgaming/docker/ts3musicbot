FROM ubuntu:18.04

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y  software-properties-common && \
    apt-get update && \
    apt-get install -y openjdk-8-jdk curl ca-certificates openssl libssl-dev git tar && \
	apt-get install -y libavcodec-extra mplayer xvfb libglib2.0-0 libxrender-dev libxrandr2 libxcursor-dev libx11-xcb-dev python ffmpeg libavcodec-extra-* xdotool x11-utils && \
	apt-get install -y libegl1-mesa libgl1-mesa-dri && \
	apt-get install --reinstall xkb-data && \
	apt-get install -y  x11-xkb-utils libnss3 libpci3 libxslt1.1 && \
    apt-get clean

RUN useradd -m -d /home/container -s /bin/bash container

USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
