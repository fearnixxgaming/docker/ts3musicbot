#!/bin/ash
# TS3MusicBot Installation Script
# ## !! USED IN PTERODACTYL !! ##
# Put this in the installation script editor within your egg configuration.
#
# Tested with ``alpine:3.4`` and ``ash``
#
# Remember: add ``-notportable`` to your start command
# Server Files: /mnt/server
apk update
apk add curl tar

VERSION="v4.5"

cd /mnt/server

curl -o TS3MusicBot.tar http://ts3musicbot.gamed.de/TS3MusicBot_${VERSION}.tar
tar --strip-components=1 -xvf TS3MusicBot.tar
